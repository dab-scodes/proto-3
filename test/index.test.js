import test from 'ava'
import { Nuxt, Builder } from 'nuxt'
import { resolve } from 'path'

// Nous gardons une référence à Nuxt pour fermer
// le serveur à la fin du test
let nuxt = null

// Initialiser Nuxt.js et démarrer l'écoute sur localhost:4000
test.before('Init Nuxt.js', async t => {
  const rootDir = resolve(__dirname, '..')
  let config = {}
  try { config = require(resolve(rootDir, 'nuxt.config.js')) } catch (e) {}
  config.rootDir = rootDir // dossier du projet
  config.dev = false // build de production
  config.mode = 'universal' // application isomorphique
  nuxt = new Nuxt(config)
  await new Builder(nuxt).build()
  nuxt.listen(4010, 'localhost')
})

// Exemple de test uniquement sur le HTML généré
test('Route / exits and render HTML', async t => {
  let context = {}
  const { html } = await nuxt.renderRoute('/', context)
  t.true(html.includes('Ipfs'))
})

// Exemple de test via la vérification du DOM
/*test('Route / exits and render HTML with CSS applied', async t => {
  const window = await nuxt.renderAndGetWindow('http://localhost:4010/')
  console.log(window);
  const element = window.document.querySelector('.red')
  t.not(element, null)
  t.is(element.textContent, 'Accueil')
  t.is(element.className, 'red')
  t.is(window.getComputedStyle(element).color, 'red')
})*/

// Arrêter le serveur Nuxt
test.after('Closing server', t => {
  nuxt.close()
})
